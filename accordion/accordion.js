var accordion = {
	settings: {
	},
	init: function() {
		s = this.settings;
		$(".accordion li:not(.active) .content").hide();
		this.bindUIActions();
	},
	bindUIActions: function() {
		$("body").on("click", ".accordion li", accordion.handlers.accordionToggle);
	},
	handlers: {
		accordionToggle: function () {
			"use strict";
			var $li = $(this),
				isThisActive = $li.hasClass("active"),
				$active;
			if ( isThisActive ) {
				$li.find(".btnAccordionToggle").text("+");
				$li.removeClass("active").children(".content").hide("fast");
			} else {
				$active = $li.siblings(".active");
				$active.find(".btnAccordionToggle").text("+");
				$active.removeClass("active").children(".content").hide("fast");
				$li.addClass("active").children(".content").show("fast");
				$li.find(".btnAccordionToggle").text("-");
			}
		}
	}
}

$(document).ready(function(){
	accordion.init();
});