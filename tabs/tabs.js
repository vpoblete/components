$(function () {
	var tabs = {
		bindUIActions: function() {
			$(".tabs").on("click", "li", tabs.handlers.tabToggle);
		},
		handlers: {
			tabToggle: function () {
				var $tab = $(this),
					$tabsContent = $tab.parent().siblings(".tabsContent"),
					index = $tab.index();
				$tab.addClass("active").siblings().removeClass("active");
				$tabsContent.children(".active").removeClass("active");
				$tabsContent.children("li:nth-child(" + (index+1) + ")").addClass("active");
			}
		}
	};

	(function() {
		$(".tabs li:not(.active) .content").hide();
		tabs.bindUIActions();
	}());
});